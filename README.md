# gmc global styling npm package

## To install

```
npm install git+https://bitbucket.org/gmcdigitalwebteam/npm-gmc-global-styling.git

\*You will need to be logged in as a team member.

```

## Usage in Vue.js cli/webpack project

```

@import "~gmc-global-styling/global";

@import "~gmc-global-styling/mpts";

```

## importing from node_modules

```

settings/variables can also be imported using full node_modules path if the above does not work

```
